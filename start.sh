#!/bin/bash

echo "Starting Service"

i=20;

while [ $i -ge 0 ];
do
	printf "Starting service in $i\n";
	sleep 1;
	i=$((i-1));
done


echo "Checking for DB migrations"

java -jar target/peregrine.jar db migrate config/docker.yml

echo "Starting service"

java -jar target/peregrine.jar server config/docker.yml