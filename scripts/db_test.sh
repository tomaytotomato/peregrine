#!/usr/bin/env bash

echo "Testing DB Migrations "

echo "Building Service"

mvn clean install -DskipTests
echo "Testing DB status"

java -jar target/peregrine.jar db status config/local.yml

echo "Testing DB migration"

java -jar target/peregrine.jar db test config/local.yml
