#!/usr/bin/env bash

echo "Testing DB Migrations "

echo "Building Service"

mvn clean install -DskipTests
git config --global user.email "bruce.a.taylor88@gmail.com"
echo "Testing DB status"

java -jar ../target/peregrine.jar db migrate config/docker.yml

echo "Testing DB migration"

java -jar target/peregrine.jar db test config/local.yml
