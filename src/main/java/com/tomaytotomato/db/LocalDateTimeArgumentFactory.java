package com.tomaytotomato.db;

import io.dropwizard.jdbi.args.LocalDateTimeArgumentFactory;
import org.skife.jdbi.v2.StatementContext;
import java.time.LocalDateTime;

class NullAwareLocalDateTimeArgumentFactory extends LocalDateTimeArgumentFactory {

    @Override
    public boolean accepts(Class<?> expectedType, Object value, StatementContext ctx) {
        if(value == null){
            return LocalDateTime.class.isAssignableFrom(expectedType);
        }
        else {
            return super.accepts(expectedType, value, ctx);
        }
    }
}
