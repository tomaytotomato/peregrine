package com.tomaytotomato.db.dao;

import com.tomaytotomato.db.bindings.BindSighting;
import com.tomaytotomato.db.mappers.SightingMapper;
import com.tomaytotomato.db.models.Sighting;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.sql.SQLException;
import java.util.List;

/**
 * Manages / logs what notifications have been sent to which users
 */
public interface SightingDao {

    /**
     * Creates a new sighting record and returns the generated record id
     * @param sighting Sighting
     * @return int record id
     * @throws SQLException
     */
    @SqlUpdate("INSERT INTO sightings (species, timestamp, name, location, country, md5) "
            + "VALUES (:species, :timestamp, :name, :location, :country, :md5)")
    @GetGeneratedKeys
    Integer create(@BindSighting Sighting sighting, @Bind("md5") String md5) throws SQLException;

    /**
     * Finds a sighting record by md5 hash
     * @param md5 hash of sighting record
     * @return Sighting | null
     * @throws SQLException
     */
    @RegisterMapper(SightingMapper.class)
    @SqlQuery("SELECT * FROM sightings WHERE md5 = :md5")
    Sighting findOneByHash(@Bind("md5") String md5) throws SQLException;

    /**
     * Retrieves all sighting records
     * @return list of Sighting
     * @throws SQLException
     */
    @RegisterMapper(SightingMapper.class)
    @SqlQuery("SELECT * FROM sightings")
    List<Sighting> all() throws SQLException;
}
