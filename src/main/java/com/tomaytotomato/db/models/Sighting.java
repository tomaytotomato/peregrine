package com.tomaytotomato.db.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDateTime;

/**
 * This model defines the attributes of records stored in the in sighting table
 */
public class Sighting {

    /**
     * Record id
     */
    private Integer id;

    /**
     * Falcon species
     */
    private String species;

    /**
     * Timestamp when the sighting was submitted
     */
    private LocalDateTime timestamp;

    /**
     * Person who spotted the Falcon, or anonymous
     */
    private String name;

    /**
     * Area/region where the sighting was made
     */
    private String location;

    /**
     * The country the sighting was made in
     */
    private String country;

    /**
     * MD5 hash value of the sighting request when it was made
     *
     * i.e. md5(species+timestamp+name+location+country) = d41d8cd98f00b204e9800998ecf8427e
     *
     * Internally used to prevent duplicates
     */
    private String md5;


    public Sighting() {
    }

    public Sighting(Builder builder) {
        this.id = builder.id;
        this.species = builder.species;
        this.timestamp = builder.timestamp;
        this.name = builder.name;
        this.location = builder.location;
        this.country = builder.country;
    }

    public Integer getId() {
        return id;
    }

    public String getSpecies() {
        return species;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getCountry() {
        return country;
    }

    public String getMd5() {
        return md5;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final Sighting sighting = (Sighting) o;

        return new EqualsBuilder()
                .append(id, sighting.id)
                .append(species, sighting.species)
                .append(timestamp, sighting.timestamp)
                .append(name, sighting.name)
                .append(location, sighting.location)
                .append(country, sighting.country)
                .append(md5, sighting.md5)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(species)
                .append(timestamp)
                .append(name)
                .append(location)
                .append(country)
                .append(md5)
                .toHashCode();
    }

    public static class Builder {

        private Integer id;
        private String species;
        private LocalDateTime timestamp;
        private String name;
        private String location;
        private String country;
        private String md5;

        public Builder setId(final Integer id) {
            this.id = id;
            return this;
        }

        public Builder setSpecies(final String species) {
            this.species = species;
            return this;
        }

        public Builder setTimestamp(final LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder setName(final String name) {
            this.name = name;
            return this;
        }

        public Builder setLocation(final String location) {
            this.location = location;
            return this;
        }

        public Builder setCountry(final String country) {
            this.country = country;
            return this;
        }

        public Builder setMd5(final String md5) {
            this.md5 = md5;
            return this;
        }

        public Sighting build() {
            return new Sighting(this);
        }
    }
}
