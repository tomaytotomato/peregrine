package com.tomaytotomato.db.mappers;

import com.tomaytotomato.db.models.Sighting;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builds Sighting models from the ResultSets of {@link com.tomaytotomato.db.dao.UserDao}
 */
public class SightingMapper implements ResultSetMapper<Sighting> {

    public Sighting map(final int index, final ResultSet rs, final StatementContext ctx) throws SQLException {
        return Sighting.builder()
                .setId(rs.getInt("id"))
                .setSpecies(rs.getString("species"))
                .setTimestamp(rs.getTimestamp("timestamp").toLocalDateTime())
                .setName(rs.getString("name"))
                .setLocation(rs.getString("location"))
                .setCountry(rs.getString("country"))
                .build();
    }
}
