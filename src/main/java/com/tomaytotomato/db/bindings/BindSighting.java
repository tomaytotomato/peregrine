package com.tomaytotomato.db.bindings;

import com.tomaytotomato.db.models.Sighting;
import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import java.lang.annotation.*;

@BindingAnnotation(BindSighting.UserBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface BindSighting {

    class UserBinderFactory implements BinderFactory {

        public Binder build(Annotation annotation) {
            return new Binder<BindSighting, Sighting>() {
                public void bind(final SQLStatement q, final BindSighting bind,
                                 final Sighting sighting) {
                    q.bind("id", sighting.getId());
                    q.bind("species", sighting.getSpecies());
                    q.bind("timestamp", sighting.getTimestamp());
                    q.bind("name", sighting.getName());
                    q.bind("location", sighting.getLocation());
                    q.bind("country", sighting.getCountry());
                }
            };
        }
    }
}