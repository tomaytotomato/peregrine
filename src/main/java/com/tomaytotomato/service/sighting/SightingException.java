package com.tomaytotomato.service.sighting;

public class SightingException extends Exception {

    public SightingException() {
    }

    public SightingException(final String message) {
        super(message);
    }
}
