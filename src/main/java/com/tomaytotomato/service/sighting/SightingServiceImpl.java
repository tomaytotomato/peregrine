package com.tomaytotomato.service.sighting;

import com.tomaytotomato.api.sighting.NewSightingRequest;
import com.tomaytotomato.api.sighting.SightingResponse;
import com.tomaytotomato.db.dao.SightingDao;
import com.tomaytotomato.db.models.Sighting;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Handles the retrieval and creation of sighting and any necessary business logic
 */
public class SightingServiceImpl implements SightingService {

    private final SightingDao sightingDao;
    private final SightingServiceMapper mapper;

    public SightingServiceImpl(final SightingDao sightingDao) {
        this.sightingDao = checkNotNull(sightingDao, "Sighting DAO cannot be null");
        this.mapper = new SightingServiceMapper();
    }

    /**
     * Create a sighting record if the md5 hash of the request object does not exist in the Sightings table
     * @param request new sighting
     * @return int of the new or existing record
     * @throws SightingException
     */
    @Override
    public Integer createSighting(final NewSightingRequest request) throws SightingException {
        try {

            final String md5Digest = DigestUtils.md5Hex(request.toString());
            final Sighting sighting = sightingDao.findOneByHash(md5Digest);

            if (sighting == null) {
                return this.sightingDao.create(mapper.map(request), md5Digest);
            } else {
                return sighting.getId();
            }

        } catch (SQLException e) {
            throw new SightingException(e.getMessage());
        }
    }

    @Override
    public List<SightingResponse> getAllSightings() throws SightingException {

        try {
            return this.sightingDao.all()
                    .stream()
                    .map(mapper::map)
                    .collect(Collectors.toList());
        } catch (SQLException e) {
            throw new SightingException(e.getMessage());
        }
    }
}
