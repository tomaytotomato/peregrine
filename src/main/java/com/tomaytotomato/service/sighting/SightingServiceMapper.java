package com.tomaytotomato.service.sighting;

import com.tomaytotomato.api.sighting.NewSightingRequest;
import com.tomaytotomato.api.sighting.SightingResponse;
import com.tomaytotomato.db.models.Sighting;

/**
 * Helper class that maps any domain objects to external api objects
 */
public class SightingServiceMapper {

    public SightingResponse map(final Sighting sighting) {

        return SightingResponse.builder()
                .setId(sighting.getId())
                .setSpecies(sighting.getSpecies())
                .setName(sighting.getName())
                .setTimestamp(sighting.getTimestamp())
                .setLocation(sighting.getLocation())
                .setCountry(sighting.getCountry())
                .build();
    }

    public Sighting map(final NewSightingRequest request) {
        return Sighting.builder()
                .setSpecies(request.getSpecies())
                .setTimestamp(request.getTimestamp())
                .setName(request.getName())
                .setLocation(request.getLocation())
                .setCountry(request.getCountry())
                .build();
    }
}
