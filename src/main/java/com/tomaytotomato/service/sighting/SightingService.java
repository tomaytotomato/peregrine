package com.tomaytotomato.service.sighting;

import com.tomaytotomato.api.sighting.NewSightingRequest;
import com.tomaytotomato.api.sighting.SightingResponse;

import javax.validation.Valid;
import java.util.List;

/**
 * Domain service for sighting
 */
public interface SightingService {

    Integer createSighting(@Valid NewSightingRequest request) throws SightingException;

    List<SightingResponse> getAllSightings() throws SightingException;

}
