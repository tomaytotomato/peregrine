package com.tomaytotomato.api.sighting;

import java.util.List;

/**
 * Defines the API methods for public HTTP requests on Peregrine service
 */
public interface SightingApi {

    /**
     * Handles a request handler for a new sighting of a Falcon
     * No response given
     * @param request new sighting handler
     */
    void createSighting(NewSightingRequest request);

    /**
     * Retrieves all sighting that have been recorded on the service
     * @return list of all sighting
     */
    List<SightingResponse> getAllSightings();

}
