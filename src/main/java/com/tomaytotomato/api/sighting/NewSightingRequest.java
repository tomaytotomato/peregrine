package com.tomaytotomato.api.sighting;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Defines a public request to create a new sighting
 *
 * Note validation constraints on properties
 */
public class NewSightingRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty
    private String species;
    @NotEmpty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timestamp;
    private String name;
    @NotEmpty
    private String location;
    @NotEmpty
    private String country;

    public NewSightingRequest(){}

    public NewSightingRequest(final Builder builder) {
        this.species = builder.species;
        this.timestamp = builder.timestamp;
        this.name = builder.name;
        this.location = builder.location;
        this.country = builder.country;
    }

    public String getSpecies() {
        return species;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getCountry() {
        return country;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final NewSightingRequest that = (NewSightingRequest) o;

        return new EqualsBuilder()
                .append(species, that.species)
                .append(timestamp, that.timestamp)
                .append(name, that.name)
                .append(location, that.location)
                .append(country, that.country)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(species)
                .append(timestamp)
                .append(name)
                .append(location)
                .append(country)
                .toHashCode();
    }

    public static class Builder {

        private String species;
        private LocalDateTime timestamp;
        private String name;
        private String location;
        private String country;

        public Builder setSpecies(final String species) {
            this.species = species;
            return this;

        }

        public Builder setTimestamp(final LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;

        }

        public Builder setName(final String name) {
            this.name = name;
            return this;
        }

        public Builder setLocation(final String location) {
            this.location = location;
            return this;
        }

        public Builder setCountry(final String country) {
            this.country = country;
            return this;
        }

        public NewSightingRequest build() {
            return new NewSightingRequest(this);
        }
    }
}
