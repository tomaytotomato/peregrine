package com.tomaytotomato.api.sighting;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDateTime;

/**
 * Provides public details on each sighting record
 */
public class SightingResponse {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String species;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timestamp;
    private String name;
    private String location;
    private String country;

    public SightingResponse() {}

    public SightingResponse(final Builder builder) {
        this.id = builder.id;
        this.species = builder.species;
        this.timestamp = builder.timestamp;
        this.name = builder.name;
        this.location = builder.location;
        this.country = builder.country;
    }

    public Integer getId() {
        return id;
    }

    public String getSpecies() {
        return species;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final SightingResponse that = (SightingResponse) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(species, that.species)
                .append(timestamp, that.timestamp)
                .append(name, that.name)
                .append(location, that.location)
                .append(country, that.country)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(species)
                .append(timestamp)
                .append(name)
                .append(location)
                .append(country)
                .toHashCode();
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static class Builder {

        private Integer id;
        private String species;
        private LocalDateTime timestamp;
        private String name;
        private String location;
        private String country;

        public Builder setId(final Integer id) {
            this.id = id;
            return this;
        }

        public Builder setSpecies(final String species) {
            this.species = species;
            return this;

        }

        public Builder setTimestamp(final LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;

        }

        public Builder setName(final String name) {
            this.name = name;
            return this;
        }

        public Builder setLocation(final String location) {
            this.location = location;
            return this;
        }

        public Builder setCountry(final String country) {
            this.country = country;
            return this;
        }

        public SightingResponse build() {
            return new SightingResponse(this);
        }
    }
}
