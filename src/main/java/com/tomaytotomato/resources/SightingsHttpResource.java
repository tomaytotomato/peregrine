package com.tomaytotomato.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tomaytotomato.api.sighting.NewSightingRequest;
import com.tomaytotomato.api.sighting.SightingApi;
import com.tomaytotomato.api.sighting.SightingResponse;
import com.tomaytotomato.core.queue.redis.RedisQueueProducer;
import com.tomaytotomato.core.request.RequestLogger;
import com.tomaytotomato.service.sighting.SightingException;
import com.tomaytotomato.service.sighting.SightingService;
import io.swagger.annotations.Api;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * HTTP implementation of SightingApi
 */
@Api
@Path("/sightings")
public class SightingsHttpResource implements SightingApi {

    private final SightingService delegate;
    private final String QUEUE_CHANNEL = "sighting";
    private final RedisQueueProducer queueProducer;
    private final RequestLogger requestLogger;
    private final ObjectMapper objectMapper;

    public SightingsHttpResource(final SightingService sightingService,
                                 final RedisQueueProducer queueProducer,
                                 final RequestLogger requestLogger) {
        this.delegate = checkNotNull(sightingService, "Service cannot be null");
        this.queueProducer = checkNotNull(queueProducer, "Queue Producer cannot be null");
        this.requestLogger = checkNotNull(requestLogger, "Request logger cannot be null");
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Checks if the request has been logged before in the Redis requests set
     * If it has not been logged, then pass the request to the delegate
     * @param request new sighting handler
     */
    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    public void createSighting(final NewSightingRequest request) {
        try {
            if(!requestLogger.requestExists(request.toString())) {
                this.queueProducer.publish(QUEUE_CHANNEL, this.objectMapper.writeValueAsString(request));
                this.requestLogger.storeRequest(request.toString());
            }

        } catch (JsonProcessingException e) {
            throw new BadRequestException(e.getCause());
        }
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<SightingResponse> getAllSightings() {
        try {
            return delegate.getAllSightings();
        } catch (SightingException e) {
            throw new RuntimeException(e.getCause());
        }
    }
}
