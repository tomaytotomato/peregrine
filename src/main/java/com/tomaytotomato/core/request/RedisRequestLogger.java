package com.tomaytotomato.core.request;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Simple utility class that persists and fetches requests to a Redis set
 * <p>
 * Allowing logging / caching and preventing spamming of requests to the service(s)
 */
public class RedisRequestLogger implements RequestLogger {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final String DEFAULT_REQUEST_SET = "requests";
    private final Jedis jedis;
    private final String requestSetName;

    /**
     * Initialise with default request channel
     *
     * @param jedis
     */
    public RedisRequestLogger(final Jedis jedis) {
        this.jedis = checkNotNull(jedis, "Jedis cannot be null");
        this.requestSetName = DEFAULT_REQUEST_SET;
    }

    public RedisRequestLogger(final Jedis jedis, final String requestSetName) {
        this.jedis = checkNotNull(jedis, "Jedis cannot be null");
        this.requestSetName = checkNotNull(requestSetName, "Request set name must be specified and not null");
    }

    /**
     * Checks if the request message exists in request set
     *
     * Note uses MD5 hash of the request message as the value in the set
     *
     * @param requestMessage string
     * @return
     */
    @Override
    public boolean requestExists(final String requestMessage) {
        final String requestHash = DigestUtils.md5Hex(requestMessage.getBytes());
        return this.jedis.sismember(requestSetName, requestHash);
    }

    /**
     * Persists the request message to the request log set
     *
     * Note uses MD5 hash to generate the set element value
     *
     * @param requestMessage string
     */
    @Override
    public void storeRequest(final String requestMessage) {
        final String requestHash = DigestUtils.md5Hex(requestMessage.getBytes());
        this.jedis.sadd(requestSetName, requestHash);
    }
}
