package com.tomaytotomato.core.request;

/**
 * Defines the contract methods for the RequestLogger API
 */
public interface RequestLogger {

    /**
     * Check if a request has already been processed
     * @param requestMessage string
     * @return true if it exists
     */
    public boolean requestExists(String requestMessage);

    /**
     * Store the request in the log
     * @param requestMessage string
     */
    public void storeRequest(String requestMessage);
}
