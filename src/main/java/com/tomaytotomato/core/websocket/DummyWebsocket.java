package com.tomaytotomato.core.websocket;

import javax.websocket.server.ServerEndpoint;

/**
 * The dropwizard websocket library used does not support bundle initialisation
 * with a zero arg constructor. This dummy class is used as a workaround for this
 * during the initialization stage in dropwizard server.
 *
 * Issue reported - https://github.com/LivePersonInc/dropwizard-websockets/issues/24
 *
 */
@ServerEndpoint("/dummy")
public class DummyWebsocket {
}
