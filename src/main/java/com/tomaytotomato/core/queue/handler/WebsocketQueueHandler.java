package com.tomaytotomato.core.queue.handler;

import com.tomaytotomato.core.queue.redis.RedisQueueConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisPool;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;

/**
 * Websocket resource created for each connection which subscribes
 * to the specified websocket queue channel.
 * <p>
 * Then relays any queue messages to the websocket client
 * <p>
 * <p>
 */
@ServerEndpoint("/websocket")
public class WebsocketQueueHandler implements QueueHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Session userSession;
    private RedisQueueConsumer queueConsumer;
    private boolean subscriberRunning = false;

    /**
     * When a new client connects check if the queue subscriber is running
     * <p>
     * If not then create an instance of it
     * <p>
     * Then add their session details to the active sessions map
     *
     * @param session Session
     * @throws IOException
     */
    @OnOpen
    public void onOpen(final Session session) throws IOException {

        if (!subscriberRunning) {
            logger.info("Queue subscriber was not initialised");
            final Map<String, Object> props = session.getUserProperties();

            queueConsumer = new RedisQueueConsumer(
                    (JedisPool) props.get(JedisPool.class.getName()),
                    (String) props.get("queueChannel"),
                    this
            );

            queueConsumer.run();
            subscriberRunning = true;
        }

        this.userSession = session;
        logger.info("Client connected to sighting websocket");
    }

    @OnMessage
    public void onMessage(final Session session, final String message) {
    }

    @OnClose
    public void onClose(final Session session, final CloseReason closeReason) {
        try {
            this.userSession.close();
            this.queueConsumer.stop();
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        }
        logger.info("Client disconnected : " + closeReason.toString());
    }

    @OnError
    public void onError(final Session session, final Throwable error) {
        logger.error(error.getLocalizedMessage());
    }

    /**
     * @param message string
     */
    @Override
    public void handleMessage(final String message) {
        this.userSession.getAsyncRemote().sendText(message);

    }
}