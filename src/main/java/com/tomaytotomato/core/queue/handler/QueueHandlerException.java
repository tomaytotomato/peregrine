package com.tomaytotomato.core.queue.handler;

/**
 * General exception for Queue Handlers
 */
public class QueueHandlerException extends Exception {

    public QueueHandlerException(final String message) {
        super(message);
    }
}
