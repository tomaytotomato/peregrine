package com.tomaytotomato.core.queue.handler;

/**
 * Business logic for Queue events
 */
public interface QueueHandler {

    void handleMessage(String message) throws QueueHandlerException;

}
