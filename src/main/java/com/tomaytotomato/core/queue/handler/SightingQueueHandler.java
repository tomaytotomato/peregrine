package com.tomaytotomato.core.queue.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tomaytotomato.api.sighting.NewSightingRequest;
import com.tomaytotomato.core.queue.redis.RedisQueueProducer;
import com.tomaytotomato.service.sighting.SightingException;
import com.tomaytotomato.service.sighting.SightingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Business logic for handling sighting messages published in the queue
 */
public class SightingQueueHandler implements QueueHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final SightingService sightingService;
    private final RedisQueueProducer queueProducer;
    private final ObjectMapper objectMapper;

    public SightingQueueHandler(final SightingService sightingService, final RedisQueueProducer queueProducer) {
        this.sightingService = checkNotNull(sightingService, "Sighting service cannot be null");
        this.queueProducer = checkNotNull(queueProducer, "Queue producer must be initialised");
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Handles the serialized handler into a NewSightingRequest
     * then persists it to the DB and finally dispatches a notification to any
     * subscribed clients on the server's websocket
     * @param serializedMessage string
     * @throws QueueHandlerException
     */
    @Override
    public void handleMessage(final String serializedMessage) throws QueueHandlerException {

        logger.info("Handling sighting queue handler");
        try {
            final NewSightingRequest request = objectMapper.readValue(serializedMessage, NewSightingRequest.class);

            this.sightingService.createSighting(request);

            final String message = request.getName() +
                    " has spotted a " +
                    request.getSpecies() +
                    " in " +
                    request.getLocation() +
                    ", " +
                    request.getCountry();

            this.queueProducer.publish("websocket", message);
            logger.info("Completed handling of sighting queue handler");


        } catch (IOException | SightingException e) {
            logger.error(e.getLocalizedMessage());
            throw new QueueHandlerException(e.getMessage());
        }
    }
}
