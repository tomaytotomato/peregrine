package com.tomaytotomato.core.queue.redis;

import com.tomaytotomato.core.queue.handler.QueueHandler;
import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.exceptions.JedisException;

import javax.ws.rs.core.Context;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This class is responsible for listening to a specific redis redis and then performing
 * business logic to handle events on that redis channel
 */
public class RedisQueueConsumer extends JedisPubSub implements Managed, Runnable {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    final private String queueChannel;
    final private JedisPool jedisPool;
    private final long startMillis = System.currentTimeMillis();
    private final QueueHandler queueHandler;

    public RedisQueueConsumer(@Context final JedisPool pool, final String queueChannel,
                              final QueueHandler queueHandler) {
        this.jedisPool = checkNotNull(pool, "Jedis Pool must be initalised");
        this.queueChannel = checkNotNull(queueChannel, "Queue channel must be set to subscribe to");
        this.queueHandler = checkNotNull(queueHandler, "Queue Handler must be iitialised");
    }

    /**
     * On thread initialisation subscribe to the particular redis redis channel
     * <p>
     * Note: this is blocking while polling the redis
     */
    @Override
    public void run() {

        final Jedis subscriber = this.jedisPool.getResource();
        final Thread thread = new Thread(() -> {
        while (true) {
            try {
                subscriber.subscribe(this, queueChannel);
                logger.info("Subscribed to redis channel : " + queueChannel);
            } catch (JedisException e) {
                logger.error(e.getLocalizedMessage());
            }
        }});

        thread.start();
    }

    /**
     * All logic for handling messages that are published to a particular queue channel
     * @param channel queue channel string
     * @param message handler string
     */
    @Override
    public void onMessage(String channel, String message)
    {
        try {
            queueHandler.handleMessage(message);
            logger.info("Received handler on channel : " + channel + " - content : " + message);
        } catch (Exception ex) {
            logger.error("Failed to handle handler " + message + " on channel " + channel);
        }
    }

    private void log(String string, Object... args) {
        long millisSinceStart = System.currentTimeMillis() - startMillis;
        logger.info(Thread.currentThread().getName() + " " + millisSinceStart + " " + String.format(string, args));
    }

    @Override
    public void start() throws Exception {
        logger.info("Starting queue consumer on channel : " + queueChannel);
        this.run();
    }

    @Override
    public void stop() throws Exception {
        logger.info("Stopping queue consumer on channel : " + queueChannel);
    }
}
