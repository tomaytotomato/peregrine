package com.tomaytotomato.core.queue.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

import javax.ws.rs.core.Context;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A Websocket service that also subscribes to a Redis Queue to push messages
 * from Redis to Websocket clients
 */
public class RedisQueueConsumerWebSocket extends JedisPubSub implements Runnable {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    final private String queueChannel;
    final private JedisPool jedisPool;

    public RedisQueueConsumerWebSocket(@Context final JedisPool pool, final String queueChannel) {
        this.jedisPool = checkNotNull(pool, "Jedis Pool must be initalised");
        this.queueChannel = checkNotNull(queueChannel, "Queue channel must be set to subscribe to");
    }



    @Override
    public void run() {

    }
}
