package com.tomaytotomato.core.queue.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

import javax.ws.rs.core.Context;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This class allows messages to be published to redis queues
 */
public class RedisQueueProducer {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    final private Jedis jedis;

    public RedisQueueProducer(@Context final Jedis jedis) {
        this.jedis = checkNotNull(jedis, "Jedis cannot be null");
    }

    /**
     * Publishes a handler to the specified redis queue
     *
     * Note using Publish and not List Push (LPUSH) to guarantee deliverability
     * incase subscriber is not ready
     * @param queueChannel channel name
     * @param message content as string
     * @throws JedisException
     */
    public void publish(final String queueChannel, final String message) throws JedisException {
        this.jedis.publish(queueChannel, message);
        logger.info("Published message : " + message + " to channel : " + queueChannel);
    }
}
