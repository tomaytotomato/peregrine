package com.tomaytotomato;

import com.bendb.dropwizard.redis.JedisBundle;
import com.bendb.dropwizard.redis.JedisFactory;
import com.tomaytotomato.core.queue.handler.QueueHandler;
import com.tomaytotomato.core.queue.handler.SightingQueueHandler;
import com.tomaytotomato.core.queue.handler.WebsocketQueueHandler;
import com.tomaytotomato.core.queue.redis.RedisQueueConsumer;
import com.tomaytotomato.core.queue.redis.RedisQueueProducer;
import com.tomaytotomato.core.request.RedisRequestLogger;
import com.tomaytotomato.core.request.RequestLogger;
import com.tomaytotomato.core.websocket.DummyWebsocket;
import com.tomaytotomato.db.dao.SightingDao;
import com.tomaytotomato.resources.SightingsHttpResource;
import com.tomaytotomato.service.sighting.SightingService;
import com.tomaytotomato.service.sighting.SightingServiceImpl;
import io.dropwizard.Application;
import io.dropwizard.bundles.assets.ConfiguredAssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.jdbi.args.LocalDateTimeArgumentFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.websockets.WebsocketBundle;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.eclipse.jetty.websocket.jsr356.server.BasicServerEndpointConfig;
import org.skife.jdbi.v2.DBI;
import redis.clients.jedis.JedisPool;

import javax.websocket.server.ServerEndpointConfig;

public class ServiceApplication extends Application<ServiceConfiguration> {

    private WebsocketBundle websocketBundle;


    public static void main(final String[] args) throws Exception {
        new ServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "Peregrine Service";
    }

    @Override
    public void initialize(final Bootstrap<ServiceConfiguration> bootstrap) {

        bootstrap.addBundle(new SwaggerBundle<ServiceConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(
                    ServiceConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });

        bootstrap.addBundle(new JedisBundle<ServiceConfiguration>() {
            @Override
            public JedisFactory getJedisFactory(ServiceConfiguration configuration) {
                return configuration.getJedisFactory();
            }
        });

        websocketBundle = new WebsocketBundle(DummyWebsocket.class);
        bootstrap.addBundle(websocketBundle);

        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor()
                )
        );


        bootstrap.addBundle(new MigrationsBundle<ServiceConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(
                    ServiceConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });

        // root url loads all contents from /resources/html
        bootstrap.addBundle(new ConfiguredAssetsBundle("/html/", "/", "index.html"));

    }

    @Override
    public void run(final ServiceConfiguration config,
                    final Environment environment) {

        //JDBI
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory
                .build(environment, config.getDataSourceFactory(), "service");
        jdbi.registerArgumentFactory(new LocalDateTimeArgumentFactory());
        final SightingDao sightingDao = jdbi.onDemand(SightingDao.class);

        //domain services
        final SightingService sightingService = new SightingServiceImpl(sightingDao);

        //redis
        final JedisPool pool = config.getJedisFactory().build(environment);
        final RedisQueueProducer queueProducer = new RedisQueueProducer(pool.getResource());

        final QueueHandler sightingQueueHandler = new SightingQueueHandler(sightingService, queueProducer);

        final RequestLogger requestLogger = new RedisRequestLogger(pool.getResource());

        //websocket - queue handler
        final ServerEndpointConfig wsConfig = BasicServerEndpointConfig.Builder.create(WebsocketQueueHandler.class, "/websocket").build();
        wsConfig.getUserProperties().put(JedisPool.class.getName(), pool);
        wsConfig.getUserProperties().put("queueChannel", "websocket");
        websocketBundle.addEndpoint(wsConfig);

        final RedisQueueConsumer sightingQueueConsumer =
                new RedisQueueConsumer(pool, "sighting", sightingQueueHandler);

        //HTTP Resources
        environment.jersey().register(new SightingsHttpResource(sightingService, queueProducer, requestLogger));
        environment.lifecycle().manage(sightingQueueConsumer);
    }
}
