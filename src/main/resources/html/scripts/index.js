let socketListener = new WebSocket(`ws://${location.host}/websocket`);
let previousRequest = {
    species : "",
    name : "",
    location : "",
    country : "",
};

socketListener.onopen = function (event) {
    console.log("Peregrine websocket listener connected");
};

/**
 * On websocket message from server append message to page
 * and create a notification
 * @param event
 */
socketListener.onmessage = function (event) {
    console.log(event.data);

    let sightingItemClone = document.querySelector(".liveSightingItem.invisible").cloneNode(true);
    sightingItemClone.querySelector(".liveSightingDetails").innerHTML = event.data;
    sightingItemClone.classList.remove('invisible');

    document.getElementById("liveSightingsList")
        .appendChild(sightingItemClone);

    let sightingsCount = document.getElementById("liveSightingsCount");
    sightingsCount.innerHTML = parseInt(sightingsCount.innerHTML) + 1;

    createNotification("New Falcon Sighting", event.data);
};

/**
 * Refresh connection on close
 * @param event
 */
socketListener.onclose = function (event) {
    console.log("Websocket closed");
    socketListener = new WebSocket("ws://127.0.0.1:8080/websocket");
};

function createNotification(title, body) {
    let options = {
        body: body,
    };
    let n = new Notification(title, options);
}


function previousRequestMatches(request) {

    return request.species === previousRequest.species &&
        request.name === previousRequest.name &&
        request.country === previousRequest.country &&
        request.location === previousRequest.location

}

/**
 * Once the page has fully loaded, execute this logic
 */
window.onload = function () {
    console.log("Peregrine index.js loaded");


    Notification.requestPermission().then(function (result) {
        console.log("Notifications granted on browser");
    });


    document.getElementById("getAllSightings").onclick = function () {
        console.log("Get all sightings");
        let list = "";
        fetch('api/sightings', {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                },
            },
        ).then(response => {
            if (response.ok) {
                response.json().then(json => {
                    console.log(json);
                    for (let id in json) {

                        let sighting = json[id]
                        console.log(sighting);
                        let name = "Anon";
                        if(sighting.name !== "") {
                            name = sighting.name;
                        }

                        list += `<li>${sighting.id} - ${sighting.species}  @ ${sighting.timestamp} by ${name} in ${sighting.location}, ${sighting.country}  </li>`;
                    }
                    document.getElementById("allSightings").innerHTML = list;
                });
            }
        });

    };

    document.getElementById("createSighting").onclick = function () {
        console.log("Create sighting");

        const species = document.getElementById("species").value;
        const name = document.getElementById("name").value;
        const location = document.getElementById("location").value;
        const country = document.getElementById("country").value;

        let request = {
            species: species,
            name: name,
            location: location,
            country: country
        };

        if (previousRequestMatches(request) === false) {

            request['timestamp'] = new Date().toISOString();

            fetch('/api/sightings', {
                method: 'post',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(request)
            }).then(function (response) {
                previousRequest = {
                    species: request.species,
                    name: request.name,
                    location: request.location,
                    country: request.country
                };
                console.log("Request sent successfully");
            }).then(function (data) {
            });
        }
    };

};



