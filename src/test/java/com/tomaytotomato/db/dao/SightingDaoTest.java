package com.tomaytotomato.db.dao;

import com.github.arteam.jdit.DBIRunner;
import com.github.arteam.jdit.annotations.DBIHandle;
import com.github.arteam.jdit.annotations.DataSet;
import com.github.arteam.jdit.annotations.TestedDao;
import com.tomaytotomato.db.models.Sighting;
import org.apache.commons.codec.digest.DigestUtils;
import org.jdbi.v3.core.Handle;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * DB integration test for {@link SightingDao}
 *
 * Issue with JDIT and this test
 */
@RunWith(DBIRunner.class)
@DataSet("sql/data.sql")
public class SightingDaoTest {

    @TestedDao
    private SightingDao sightingDao;

    @DBIHandle
    private Handle handle;

    /**
     * Tests the DAO with MySQL to create a new sighting record
     */
    @Test
    @Ignore
    public void testCreateSighting() throws SQLException {

        final Sighting newSighting = Sighting.builder()
                .setSpecies("Prairie Falcon")
                .setTimestamp(LocalDateTime.now())
                .setName("Johnny B Goode")
                .setLocation("Idaho")
                .setCountry("USA")
                .build();

        final String md5Hash = DigestUtils.md5Hex(newSighting.toString().getBytes());

        final Integer recordId = sightingDao.create(newSighting, md5Hash);

        assertNotNull(recordId);

        //fetch newly created sighting and compare it
        final Sighting result = sightingDao.findOneByHash(md5Hash);
        assertEquals(newSighting, result);
    }
}