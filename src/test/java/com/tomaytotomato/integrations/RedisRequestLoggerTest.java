package com.tomaytotomato.integrations;

import com.tomaytotomato.ServiceApplication;
import com.tomaytotomato.ServiceConfiguration;
import com.tomaytotomato.core.request.RedisRequestLogger;
import com.tomaytotomato.core.request.RequestLogger;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import static org.junit.Assert.*;

/**
 * Integration test for Request Logger Redis implementation
 */
public class RedisRequestLoggerTest {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config/test.yml");

    @ClassRule
    public static final DropwizardAppRule<ServiceConfiguration> RULE = new DropwizardAppRule<>(
            ServiceApplication.class, CONFIG_PATH);

    private JedisPool jedisPool;
    private RequestLogger requestLogger;
    private final String REQUEST_LOGGER_SET = "test-set";

    /**
     * Setup a Jedis instance with some sample data already existing in the test set
     */
    @Before
    public void setUp() {
        jedisPool = RULE.getConfiguration().getJedisFactory().build(RULE.getEnvironment());

        final Jedis jedis = jedisPool.getResource();

        jedis.sadd(REQUEST_LOGGER_SET, DigestUtils.md5Hex("foo".getBytes()),
                DigestUtils.md5Hex("bar".getBytes()));

        requestLogger = new RedisRequestLogger(jedis, REQUEST_LOGGER_SET);
    }

    @After
    public void tearDown() {
        jedisPool.close();
    }

    @Test
    public void requestExists() {
        assertTrue(requestLogger.requestExists("foo"));
        assertTrue(requestLogger.requestExists("bar"));
    }

    @Test
    public void requestDoesNotExist() {
        assertFalse(requestLogger.requestExists("banana pie"));
    }

    @Test
    public void storeRequest() {
        requestLogger.storeRequest("how do I turn this on");
    }
}