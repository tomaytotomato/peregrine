package com.tomaytotomato.integrations;

import com.tomaytotomato.ServiceApplication;
import com.tomaytotomato.ServiceConfiguration;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.*;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

import static org.junit.Assert.assertEquals;

/**
 * Simple integration test for Pub/Sub for Redis connection in the server
 */
public class RedisIntegrationTest {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config/test.yml");

    @ClassRule
    public static final DropwizardAppRule<ServiceConfiguration> RULE = new DropwizardAppRule<>(
            ServiceApplication.class, CONFIG_PATH);

    private JedisPool jedisPool;

    @Before
    public void setUp() {
        jedisPool = RULE.getConfiguration().getJedisFactory().build(RULE.getEnvironment());
    }

    @After
    public void tearDown() {
        jedisPool.close();
    }

    private void publish(final String channel, final String message) {
        final Thread t = new Thread(() -> {
            final Jedis jedisPubber = jedisPool.getResource();
            jedisPubber.publish(channel, message);
            jedisPubber.disconnect();

        });
        t.start();
    }

    @Test
    public void testSubscribe() {

        final String MESSAGE = "Bobby Tables";
        final String CHANNEL = "FOO";

        final Jedis jedisSubber = jedisPool.getResource();

        jedisSubber.subscribe(new JedisPubSub() {
            public void onMessage(String channel, String message) {
                assertEquals(CHANNEL, channel);
                assertEquals(MESSAGE, message);
                unsubscribe();
            }

            public void onSubscribe(String channel, int subscribedChannels) {
                assertEquals(CHANNEL, channel);
                assertEquals(1, subscribedChannels);

                // now that I'm subscribed... publish
                publish(CHANNEL, MESSAGE);
            }

            public void onUnsubscribe(String channel, int subscribedChannels) {
                assertEquals(CHANNEL, channel);
                assertEquals(0, subscribedChannels);
            }
        }, CHANNEL);
    }
}
