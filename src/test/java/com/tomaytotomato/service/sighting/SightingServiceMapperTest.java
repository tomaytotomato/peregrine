package com.tomaytotomato.service.sighting;

import com.tomaytotomato.api.sighting.NewSightingRequest;
import com.tomaytotomato.api.sighting.SightingResponse;
import com.tomaytotomato.db.models.Sighting;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class SightingServiceMapperTest {


    private SightingServiceMapper mapper;

    @Before
    public void setUp() throws Exception {
        this.mapper = new SightingServiceMapper();
    }

    /**
     * Check mapping a DB model to a API response object
     */
    @Test
    public void mapModelToResponse() {

        final Integer ID = 429;
        final String SPECIES = "Peregrine Falcon";
        final LocalDateTime TIMESTAMP = LocalDateTime.now();
        final String NAME = "Bob";
        final String LOCATION = "Yorkshire";
        final String COUNTRY = "UK";

        final SightingResponse expected = SightingResponse.builder()
                .setId(ID)
                .setSpecies(SPECIES)
                .setTimestamp(TIMESTAMP)
                .setName(NAME)
                .setLocation(LOCATION)
                .setCountry(COUNTRY)
                .build();

        final Sighting input = Sighting.builder()
                .setId(ID)
                .setSpecies(SPECIES)
                .setTimestamp(TIMESTAMP)
                .setName(NAME)
                .setLocation(LOCATION)
                .setCountry(COUNTRY)
                .build();

        //check mapping model to response
        assertEquals(expected, this.mapper.map(input));
    }

    /**
     * Check mapping a API request to a new DB model object
     **/
    @Test
    public void mapRequestToModel() {

        final String SPECIES = "Gyrfalcon";
        final LocalDateTime TIMESTAMP = LocalDateTime.now();
        final String NAME = "Sarah";
        final String LOCATION = "Bergen";
        final String COUNTRY = "Norway";

        final NewSightingRequest request = NewSightingRequest.builder()
                .setSpecies(SPECIES)
                .setTimestamp(TIMESTAMP)
                .setName(NAME)
                .setLocation(LOCATION)
                .setCountry(COUNTRY)
                .build();

        final Sighting expected = Sighting.builder()
                .setSpecies(SPECIES)
                .setTimestamp(TIMESTAMP)
                .setName(NAME)
                .setLocation(LOCATION)
                .setCountry(COUNTRY)
                .build();

        assertEquals(expected, this.mapper.map(request));
    }
}