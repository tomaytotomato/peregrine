package com.tomaytotomato.service.sighting;

import com.google.common.collect.ImmutableList;
import com.tomaytotomato.api.sighting.NewSightingRequest;
import com.tomaytotomato.api.sighting.SightingResponse;
import com.tomaytotomato.db.dao.SightingDao;
import com.tomaytotomato.db.models.Sighting;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SightingServiceImplTest {

    private SightingService sightingService;
    private SightingDao sightingDao;
    private SightingServiceMapper mapper;

    @Before
    public void setUp() throws Exception {
        this.sightingDao = mock(SightingDao.class);
        this.mapper = mock(SightingServiceMapper.class);
        this.sightingService = new SightingServiceImpl(sightingDao);
    }

    @Test
    public void createSighting() throws Exception {
        final Integer RECORD_ID = 142;
        final NewSightingRequest request = new NewSightingRequest();

        when(sightingDao.findOneByHash(any())).thenReturn(null);
        when(sightingDao.create(any(), any())).thenReturn(RECORD_ID);
        assertEquals(RECORD_ID, this.sightingService.createSighting(request));
    }

    @Test(expected = SightingException.class)
    public void createSightingException() throws Exception {

        final NewSightingRequest request = new NewSightingRequest();

        when(sightingDao.create(any(), any())).thenThrow(SQLException.class);
        this.sightingService.createSighting(request);

        verify(sightingDao, times(1)).findOneByHash(any());
    }

    @Test
    public void createSightingAlreadyExists() throws Exception {
        final Integer RECORD_ID = 0;
        final String HASH = DigestUtils.md5Hex("sdakj@(2498".getBytes());

        final Sighting sighting = Sighting.builder()
                .setId(RECORD_ID)
                .build();

        when(sightingDao.findOneByHash(HASH)).thenReturn(sighting);
        assertEquals(RECORD_ID, this.sightingService.createSighting(new NewSightingRequest()));

        verify(sightingDao, times(1)).findOneByHash(any());
    }

    @Test
    public void getAllSightings() throws Exception {

        final List<Sighting> sightings = ImmutableList.of(
                new Sighting(), new Sighting()
        );

        final List<SightingResponse> expected = ImmutableList.of(
            new SightingResponse(), new SightingResponse()
        );

        when(sightingDao.all()).thenReturn(sightings);
        when(mapper.map(new Sighting())).thenReturn(new SightingResponse());
        assertEquals(expected, this.sightingService.getAllSightings());
    }
}