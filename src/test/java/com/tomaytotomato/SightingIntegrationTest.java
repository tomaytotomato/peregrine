package com.tomaytotomato;

import com.tomaytotomato.api.sighting.NewSightingRequest;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

/**
 * Tests the Sighting resource from the client request
 */
public class SightingIntegrationTest {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config/test.yml");

    @ClassRule
    public static final DropwizardAppRule<ServiceConfiguration> RULE = new DropwizardAppRule<>(
            ServiceApplication.class, CONFIG_PATH);

    /**
     * Clears out test database and refreshes it
     * @throws Exception
     */
    @BeforeClass
    public static void migrateDb() throws Exception {
        RULE.getApplication().run("db", "rollback", "-c", "1", CONFIG_PATH);
        RULE.getApplication().run("db", "migrate", CONFIG_PATH);
    }

    @Test
    public void testCreateSighting() throws Exception {
        final Optional<NewSightingRequest> request =
                Optional.of(NewSightingRequest.builder()
                        .setSpecies("Gyrfalcon")
                        .setTimestamp(LocalDateTime.now())
                        .setName("Boris")
                        .setLocation("Jutland")
                        .setCountry("Denmark")
                        .build());

        final Response response = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/sightings")
                .request()
                .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE));

        assertEquals(response.getStatus(), HttpStatus.NO_CONTENT_204);
    }

    @Test
    public void testGetSightings() throws Exception {
        //todo
    }
}