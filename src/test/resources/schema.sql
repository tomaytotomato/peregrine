CREATE TABLE sightings
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    species varchar(40) NOT NULL,
    timestamp timestamp DEFAULT NOW() NOT NULL,
    name varchar(40),
    location varchar(40) NOT NULL,
    country varchar(40) NOT NULL,
    md5 CHAR(32) NOT NULL
);
CREATE UNIQUE INDEX sightings_id_uindex ON sightings (id);
CREATE INDEX sightings_species_index ON sightings (species);
CREATE INDEX sightings_timestamp_index ON sightings (timestamp);
CREATE UNIQUE INDEX sightings_md5_uindex ON sightings (md5);