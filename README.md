# Peregrine

Backend application test for Falcon.io

Author: Bruce Taylor <bruce.a.taylor88@gmail.com

[Dev Setup](/docs/dev.md)


## Summary

This service allows users to submit sightings of Falcons across the world.
As well as look at existing sightings and be alerted of new sightings in real-time.

### Requirements


#### REST HTTP methods

As a developer I would like to use a RESTful approach when using HTTP methods
for representing states of data.
So that HTTP methods are easily understood.

#### In memory persistence of messages before processing

As a developer I would like to persist new sighting messages in memory in a queue fashion.
So that there is minimal delay in user submission getting a response.


_Note:Redis over RabbitMQ as a queuing system?_

#### Disk persistence of messages after processing

As a developer I would like to persist JSON payloads on disk using a file based
database. 
To ensure data reliability is high and no messages are lost during downtime.

#### Realtime notifications of sightings

As a developer I will use the websocket protocol for client and server.
So that the user can view messages being submitted in realtime.

#### Frontend

As a developer I will build a simple front end for the user.
To allow them to do the following

    * Retrieve all messages submitted
    * Submit new messages
    * View other messages being submitted in realtime
    
    
## Non functional requirements

Hosting on AWS 
Local running and operation using docker compose
Publicly available via repository (github)

## High level architecture

The system consists of:

* Java service built using the Dropwizard framework
* Redis in memory database working as a queue and request logger
* MySQL database for persistence of data
* Basic HTML5 front end for submitting and polling data


![Class diagram](/docs/peregrine%20service.png "Class diagram")

## Data / Entities

For the scope of this application, the `message` concept has been implemented 
as a sighting of a Falcon bird around the world.

#### Sighting

Falcon sighting by the user, contains the following

- Species - of falcon
- Location - geographical location
- Country - e.g. UK, FR
- Timestamp - ISO format yyyy-dd-mm HH:mm:ssZ
- Name - name of the person who spotted the falcon
- Hash - md5 hash of aforementioned values, to stop duplicates

## Implementation

The Peregrine service is built using Dropwizard which uses Jetty as a container. The core of the webservice
can be split up into three layers:

## Business Logic

`getAllSightings()`

Retrieve all Falcon sightings that have been recorded by the service.

Note: will need to seed 
        

`createSighting()`

Creates a new Falcon sighting record.

    Basic validation of the request
    Store request in a queue
    
Note: another piece of logic will need to subscribe to the Queue for new sightings
and then take responsibility to persist them on MySQL

Note: a call will need to be emitted to the websocket server, to notify the client
that a new sighting was successfully received.


#### Other things

- Use an ORM? - no, it will add too much complexity for what is a very simple data model
- Security / authentication? - no, assumed trusted environment with anonymous data

## Ways to improve the service

#### Dependency Injection / Management

Using a DI library like Dagger or Guice, it would make the management of resources much clearer
and restrict the chances of resource duplication e.g. (singleton enforcement)
However since there are only a few dependencies, it is ok to manage them during the application
initialization stage.

#### Integration Tests

For the external dependencies such as MySQL, Redis. It would be useful to provide some basic
integration tests to make sure the service will be able to work after code changes.
Doing MySQL inserts and selects to a test DB etc.

Using Docker to create containers for each dependency and then interact with them.
Currently hardcoded test.yml to use Redis and MySQL running locally on my machine.

#### Queue pollution

Currently the system does not prevent user requests being spammed
into the worker queue. Meaning duplicate requests will be processed.
The sighting service currently checks if the record has already been saved
before on MySQL.


#### Metrics / monitoring

Essentially allows the DevOps people to make sure the service is working properly and generating messages
which can be displayed on pretty dashboards.

#### More unit test coverage

The never ending battle...