FROM maven:3.5.3-jdk-8-slim
MAINTAINER Bruce Taylor <bruce.a.taylor88@gmail.com>

RUN echo "Building service"

RUN mkdir /app

WORKDIR /app

VOLUME ~/.m2:/root/.m2


ADD pom.xml /app

RUN mvn clean verify -DskipTests

ADD . /app

RUN mvn package -DskipTests

EXPOSE 8080:8080