
# Getting Started

The Peregrine service is a Java service running in an embedded Jetty container
a.k.a "fat jar".

## Caveats - 

Testing of this service is not configured to use docker,
The test config file is configured to use MySQL and Redis 
on localhost, therefore integration tests will fail if those services are 
not running locally.

Also note that docker skips all unit tests during build.

I would normally use fabric8io's docker-maven plugin to handle
integration tests if I had more time.

To make the integration tests work locally, update the `test.yml` file

## Build and Deployment Steps

These steps are to get the service running locally using docker-compose for the best compatibility.

### Setting config / prepping .env file

Docker-compose makes use of an .env file to provide build specific variables, copy the existing
example file like so:

    cp .env.example .env
    
You will then need to edit the .env file with the appropriate parameters, if necessary

### Docker-compose build

Now that there are environment config values, simply run:

    docker-compose build
    
This will build the service image locally, with the appropriate `tag` set in the .env 

### Docker-compose up

Now to start the service as a container, in detached mode:

    docker-compose up -d
    
To stop the container(s)

    docker-compose down
    
### Verify its working with logs

    docker-compose logs -t -f 

    
### Deploying to AWS

To deploy the service to a remote host, we will use `docker-machine` to create an AWS instance

You will need an AWS key and token to do this.

More info can be found [here](https://docs.docker.com/machine/examples/aws/#step-1-sign-up-for-aws-and-configure-credentials)

Before you can deploy, the image must be available through a docker registry. 
To push the image to a registry you will need to use `docker-login` e.g.

    docker login registry.gitlab.com
    
Once successfully authenticated, run
    
    docker-compose push
    
Now we can deploy using docker-machine like so

    docker-machine create --driver amazonec2 --amazonec2-region eu-west-2 --amazonec2-instance-type t2.micro  peregrine

This will provision a t2.micro instance in region `eu-west-2` called peregrine

To verify the ec2 instance is ready

    docker-machine ls
    
Finally, to deploy the service container, run the following commands

    eval $(docker-machine env peregrine)
    docker-compose pull
    docker-compose up -d
    
The service is now deployed and running Yay!

# Running without Docker, the hard way!

To run it locally without a docker container, the service requires:

* Java 8

If you are using **Java 9** you will have build issues, however they can be 
fixed by following the instructions [here](https://stackoverflow.com/questions/43574426/how-to-resolve-java-lang-noclassdeffounderror-javax-xml-bind-jaxbexception-in-j?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa).

* Maven

* Mysql 5.7 service running on the host machine

## Database

The database being used is MySQL.
Liquibase is used for creating the schema.

#### Status of migrations

`java -jar target/peregrine.jar db status config/local.yml`

#### Testing the migration

`java -jar target/peregrine.jar db test config/local.yml`

#### Applying migrations

`java -jar target/peregrine.jar db migrate config/local.yml`

#### Rolling back migrations

`java -jar target/peregrine.jar db rollback -c 1 config/local.yml`

## Building

    mvn clean install
    


